public class CatalogItem { //our controller / what different inputs are biased off of
    public String title;//saved as strin
    //** defined above - public static String title; //QUESTION
    public String author; //all the qualities of of an item on the list
    public String genre;//quality
    public boolean available;//IN LIBRARY SYSTEM OR TAKEN OUT???


    public CatalogItem(String t, String a, String g) { //this is specifying/defining the parameters for the method
        title = t;
        author = a;
        genre = g;
        available = true; //this is the initial values for all of the books.
    }
       public String getTitle()
    {
        return title;
    }

    //author accessor
    public String getAuthor()
    {
        return author;
    }

    //genre accessor
    public String getGenre()
    {
        return genre; //gets genre
    }

    //availability mutator, boolean value changes
    public void markAvailable()
    {
        available = true; //was declared as boolean in variable dictionary
    }

    public boolean isAvailable() { //statement is true if it is done
        return available;
    }

    //returns a string explaining the change in the status of book in catalog
    public String toString()
    {
    //each line of text is broken into pieces as we've
    //seen above.  when marking a book as available, it prints each individual aspect
    //of the book entry (title, author, genre, serialNum)
    //and then asks if the user if the task is completed.
        return new String(title + ", " + author + ", " + genre + ", available? " + available);
    }

}
