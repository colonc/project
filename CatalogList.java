import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException; //keeps our file going through a possible break in the program

public class CatalogList {

    //defined variables
    private ArrayList<CatalogItem> catalogItems;
    private static final String CATALOGFILE = "catalog.txt"; //the new name for our text file the has our task list

    public CatalogList() {
        catalogItems = new ArrayList<CatalogItem>(); //our method, calling the TodoItem.java

    }

    public void addCatalogItem(CatalogItem catalogItem)
    {
        catalogItems.add(catalogItem);
    }

    public Iterator getCatalogItems()
    {
        return catalogItems.iterator();
    }

    public void readCatalogItemsFromFile() throws IOException { //going to our text file to read tasks in list
        Scanner fileScanner = new Scanner(new File(CATALOGFILE));
        while(fileScanner.hasNext()) { //runs the number of total tasks in list
            String catalogItemLine = fileScanner.nextLine();
            Scanner catalogScanner = new Scanner(catalogItemLine);
            catalogScanner.useDelimiter(", "); //tells the scanner to break up the entered tasks based on the commas in the statement
            String title, author, genre, avaliable; //this is how the Scanner is to store each piece of the information
            title = catalogScanner.next(); //scanner reads the priority piece of the task
            author = catalogScanner.next(); //scanner reads the category piece of the task
            genre = catalogScanner.next(); //scanner reads the task piece of the task
            avaliable = catalogScanner.next();
            CatalogItem catalogItem = new CatalogItem(title, author, genre); //goes to CatalogItem and reads the method and its parameters
            catalogItems.add(catalogItem);
        }
        System.out.println(catalogItems);
    }

    public void markBookAsAvailable(int toMarkAvailable)
    {
        Iterator iterator = catalogItems.iterator(); //the iterator deals with groups and lists of information
        while(iterator.hasNext())
        { //begins the loop
            CatalogItem catalogItem = (CatalogItem)iterator.next(); //goes to CatalogItem.java to find method named TodoItem
            /** there is no method called getAvailable in the CatalogItem, so I commented out the if statement for now
              you will have to add method getAvailable() and add a functionality for serial numbers
              if(catalogItem.getAvailable() == toMarkAvailable)
              { //sets conditions in which to continue the loop
              catalogItem.markAvailable(); //action declared in CatalogItem
              }
             **/
        }
    }

    public Iterator findBooksWithAuthor(String requestedAuthor) //going to TodoList to find the constructor
    {
        Iterator iterator = catalogItems.iterator(); //deals with lists
        ArrayList<CatalogItem> authorList = new ArrayList<CatalogItem>();
        while(iterator.hasNext()) //getting next aspect
        {
            CatalogItem catalogItem = (CatalogItem)iterator.next();
            if(catalogItem.getAuthor().equals(requestedAuthor)) //getPriority in TodoItem
            {
                //** should this be add, not list? there is no list method in the ArrayList
                authorList.add(catalogItem);
            }
        }
        return authorList.iterator(); //returns the priority
    }

    public Iterator findBooksOfTitle(String requestedTitle)
    {
        Iterator iterator = catalogItems.iterator();

        /** not needed ? - don't see serial number info anywhere
          else if(command.equals("serialNum")) //***
          {
          System.out.println("What is the serial number of the book you are searching for?");
          int s = scanner.nextInt();
          Iterator serialList = catalogList.findBooksWithSerialNum();
          while (serialList.hasNext())
          {
          System.out.println(serialList.next());
          }
          tor = catalogItems.iterator();
         **/

        //** categoryList should be titleList, since that's how it is used below?
        ArrayList<CatalogItem> titleList = new ArrayList<CatalogItem>();
        while(iterator.hasNext())//while there is something next in the list
        {
            CatalogItem catalogItem = (CatalogItem)iterator.next(); //go through each string
            if(catalogItem.getTitle().equals(requestedTitle)) //with the specified category
            {
                //** should this be add, not list? there is no list method in the ArrayList
                titleList.add(catalogItem);
            }
        }
        return titleList.iterator();
    }

    public Iterator findBooksWithGenre (String requestedGenre)
    {
        Iterator iterator = catalogItems.iterator(); //deals with lists
        ArrayList<CatalogItem> genreList = new ArrayList<CatalogItem>();
        while(iterator.hasNext()) //getting next aspect
        {
            CatalogItem catalogItem = (CatalogItem)iterator.next();
            if(catalogItem.getGenre().equals(requestedGenre))
            {
                //** should this be add, not list? there is no list method in the ArrayList

                genreList.add(catalogItem);
            }
        }

        return genreList.iterator(); //returns the priority
    }
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = catalogItems.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString()); //goes to text file
            if(iterator.hasNext()) {
                buffer.append("\n");//new line
            }
        }
        return buffer.toString(); //returns strings from txt file
    }


}
