import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class CatalogListMain //this program is what is run, the main program
{

    public static void main(String[] args) throws IOException
    {
        System.out.println("Welcome to Cassandra and Lynsey's library!");
        System.out.println("What are you looking to find?");
        System.out.println("Available options: listBooks, title, author, genre, available, quit");

        Scanner scanner = new Scanner(System.in);
        CatalogList catalogList = new CatalogList();//own method

        while(scanner.hasNext())
        {
            String command = scanner.nextLine();
            if(command.equals("listBooks"))
            {
                catalogList.readCatalogItemsFromFile();
                System.out.println(catalogList.toString());
                //utilizing above method to read
            }
            else if(command.equals("title"))
            {
                System.out.println("What is the title of the book you are searching for?");
		//** should use nextLine, not next, otherwise will only read 1 word
                String t = scanner.nextLine();
                //** forgot to pass t as a parameter
                Iterator titleList = catalogList.findBooksOfTitle(t);
                while (titleList.hasNext())
                {
                    System.out.println(titleList.next());
                }
            }
            else if(command.equals("author"))
            {
                System.out.println("Who is the author of the book you are searching for?");
		//** should use nextLine, not next, otherwise will only read 1 word
                String a = scanner.nextLine();
		//** forgot to pass a as a parameter
                Iterator authorList = catalogList.findBooksWithAuthor(a);
                while (authorList.hasNext())
                {
                    System.out.println(authorList.next());
                }
            }
            else if(command.equals("genre"))
            {
                System.out.println("What is the genre of the book you are searching for?");
//** should use nextLine, not next, otherwise will only read 1 word
                String g = scanner.nextLine();
		//** forgot to pass g as a parameter
                Iterator genreList = catalogList.findBooksWithGenre(g);
                while (genreList.hasNext())
                {
                    System.out.println(genreList.next());
                }
            }


            else if(command.equals("available"))
            { //boolean part of loop states what to do if the task is marked as done
                System.out.println("What is the serial number?");
                int chosenSerial = scanner.nextInt();
                catalogList.markBookAsAvailable(chosenSerial); //pulls this from TodoList
            }

            else if(command.equals("quit"))
            { //when quit is printed, the program will end, stopping the loop
                break;
            }
        }

    }

}
